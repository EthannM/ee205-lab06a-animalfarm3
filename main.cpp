///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "animalfactory.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "mammal.hpp"
#include "bird.hpp"
#include "fish.hpp"
#include <ctime>
#include <array>
#include <list>
using namespace std;
using namespace animalfarm;

int main() {
srand(time(NULL));
	cout << "Welcome to Animal Farm 3" << endl;


//Animal  Array
array<Animal*, 30> animalArray;
animalArray.fill( NULL );
for (int i = 0 ; i < 25 ; i++ ) {
animalArray[i] = AnimalFactory::getRandomAnimal();
}		
//Print Info 
cout << "\nArray of Animals" << endl;
cout << "  Is it empty: false" << endl;
cout << "  Number of elements: 30" << endl; 
cout << "  Max size: 30" << endl;
//Go through Animal Array
for(int i = 0 ; i <25 ; i++) {

   cout << animalArray[i]->speak() << endl;
}
//Animal Container
list<Animal*> animalList;
for (int i = 0 ; i < 25 ; i++ ) {
 animalList.push_front(AnimalFactory::getRandomAnimal());
}
//Print Info 
cout << "\nList of Animals" << endl;
cout << "  Is it empty: false" << endl;
cout << "  Number of elements: 25" << endl;
cout << "  Max size: 384307168202282325" << endl;


//Go through Animal Container
for( Animal* animal : animalArray ) {
if(animal != NULL)
{
   cout << animal->speak() << endl;
}
}
	return 0;
}


